<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress-one' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'd1*c?3my9]RX).j@A#1@>k>KY/T5Abr4se&>e9U`t@):Ohl_&b@7,lpEgp]ghRY$' );
define( 'SECURE_AUTH_KEY',   'gCN,vVNkBEU2f.+Tixbuq!43O_Y*T0Eht.pX1JS^Nin5-f(uh k9:{iz-A7&L#&t' );
define( 'LOGGED_IN_KEY',     'zR(~h+mSf$3WL5Y#.(tCtMxj:_og{A,3)LKqO*.qvv>C[=?y=wwId$vfb#kdsb?t' );
define( 'NONCE_KEY',         'V Kb;n:59,>6&|Y3@Ng~Zw!ZYZWvLGzqKP7o)X^vCIi9CPCbpb1SYh`Q_YgrKCR~' );
define( 'AUTH_SALT',         'Fv:;4r2HQqY2Q,oEKR`Yoy<0#r@8J|>)at=_Vk*BL76M[SXb8}^8!TSvD5c3%yuh' );
define( 'SECURE_AUTH_SALT',  'Lt(Dr=F{@nEzBpVn0y~7qxUS6e[O2)!R,9kjnzrlbY71Iu]u3}NeQJheXe%nxY=c' );
define( 'LOGGED_IN_SALT',    'gjKYGz1:=M;$kC.P$A?&0VQqz;w3&q``*0S O& +s[-cW=HtC 32v7%R j[htr*3' );
define( 'NONCE_SALT',        '(P.^sl@VX:/67SXRGJ}hI3-??Wg;{GwgQh;&:0D?;XWjTQBF>jW# u(@8eo< AFB' );
define( 'WP_CACHE_KEY_SALT', 'bh>Ki1h:<>]M!ODi7Cuxr1kg[ -_Ua6uy{|GEl~s-|;tbO_W^&]XNvE-hs9?Z??1' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', True );
define( 'SCRIPT_DEBUG', true );


define( 'WP_DEBUG_LOG', True );
define( 'WP_DISABLE_FATAL_ERROR_HANDLER', True );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
